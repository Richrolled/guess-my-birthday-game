#import random functions
from random import randint

#asks for and saves name
name = input("Hi! What is your name? ")

#for loop that runs a number of up to 5 times
for attempts in range(5) :

    #generate a random month and year for each loop
    month = randint(1, 12)
    year = randint(1924, 2004)

    #prints response giving the attempt number, name, month, and year
    print("Guess", attempts + 1, ":", name + " were you born in ", str(month), "/", str(year) + "?")

    #ask whether the guess was right or wrong
    answer = input("yes or no? ")

    ##if statement giving options
    #checks if answer was correct, says yes and ends the program
    if answer == "yes":
        print("I knew it!")
        exit()
    #checks whether it is the last attempt and the answer is no
    elif attempts == 4 and answer == "no":
        print("I have other things to do. Good bye.")
    #all other cases
    else:
        print("Drat! Lemme try again!")
